import machine
import usocket as socket
import ure
import utime as time
from network import WLAN, STA_IF

led = machine.Pin(2, machine.Pin.OUT)

lamp = machine.Pin(12, machine.Pin.OUT)
fan = machine.Pin(13, machine.Pin.OUT)
button = machine.Pin(14, machine.Pin.IN, machine.Pin.PULL_UP)

html = """<!DOCTYPE html><html><head><meta name="viewport" content="widht=device-widht, initial-scale=1.0"><style>a:link, a:visited{border: none; padding: 16px 0; text-align: center; text-decoration: none; display: block; font-size: 16px; margin: 10px 0; transition-duration: 0.4s; cursor: pointer; width: 50%; background-color: white; color: black; border: 2px solid #008CBA;}a:hover, a:active{background-color: #008CBA; color: white;}</style><title>Stop Kontak</title></head><body> <h1 align="center">Panel Kontrol</h1> <p align="center"> <a href="/sw1">Stop Kontak 1</a> <a href="/sw2">Stop Kontak 2</a> <a href="/reboot">Reboot</a> </p></body></html>"""

#sta_if = network.WLAN(network.STA_IF);sta_if.config(dhcp_hostname="saklar")
def wifi_connect():
	led.off()
	wlan = WLAN(STA_IF)
	if not wlan.isconnected():
		wlan.active(True)
		wlan.connect("Redmi", "Kero.007")

		while not wlan.isconnected():
			machine.idle()
			pass
	time.sleep(5)
	print(wlan.ifconfig())
	led.on()

def parseURL(url):
	parameters = {}
	path = ure.search("(.*?)(\?|$)", url)
	while True:
		vars = ure.search("(([a-z0-9]+)=([a-z0-8.]*))&?", url)
		if vars:
			parameters[vars.group(2)] = vars.group(3)
			url = url.replace(vars.group(0), '')
		else:
			break
	return path.group(1), parameters

def buildResponse(response):
	global html
	return '''HTTP/1.0 200 OK\r\nContent-type: text/html\r\nContent-length: %d\r\n\r\n%s''' % (len(html), html)

led.off()
lamp.off()
fan.off()
wifi_connect()

addr = socket.getaddrinfo('0.0.0.0', 80)[0][-1]
s = socket.socket()
s.bind(addr)
s.listen(1)
print('listening on', addr)

while True:
	cl, addr = s.accept()
	print('client connected from', addr)

	request = str(cl.recv(1024))
	print("REQUEST: ", request)
	
	obj = ure.search("GET (.*?) HTTP\/1\.1", request)
	#print(obj.group(1))

	if not obj:
		cl.send(buildResponse("INVALID REQUEST"))
	
	else:
		path, parameters = parseURL(obj.group(1))

		if path.startswith("/sw1"):
			cl.send(buildResponse("Toggle lampu"))
			lamp(not lamp())
			
		elif path.startswith("/sw2"):
			cl.send(buildResponse("Toggle kipas"))
			fan(not fan())

		elif path.startswith("/reboot"):
			cl.send(buildResponse("Reboot wemos\n"))
			machine.reset()

		else:
			cl.send(buildResponse("Aksi belum terdaftar\r\nPATH: %s\r\nPARAMETERS: %s" % (path, parameters)))
				

	cl.close()

